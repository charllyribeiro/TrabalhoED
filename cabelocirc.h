#define MAX 5

typedef struct cabelo{
   char cor[20];
   char tipo[20];
   char tam[20];  
}Cabelo;

typedef struct fila{
	Cabelo cliente[MAX];
	int inicio;
	int fim;
	int qtd;
}Fila;

void inicia(Fila *f){
	f->inicio = 0;
	f->fim = -1;
	f->qtd = 0;
}
void insere(Fila *f, char cor[], char tipo[], char tam[]){
	if ( f->fim == MAX-1 && f->inicio > 0){
	
		f->fim = -1;
	}
	if (f->qtd == MAX){
		printf("##############\n");
		printf("##Fila Cheia##\n");
		printf("##############\n");
	}else{
		Cabelo newCliente;
		
		strcpy(newCliente.cor,cor);
		strcpy(newCliente.tipo,tipo);
		strcpy(newCliente.tam,tam);
		
		f->fim++;
		f->cliente[f->fim] = newCliente;
		f->qtd++;
		
	}
}
void retira(Fila *f){
	
	
	if (f->qtd == 0){
		printf("##############\n");
		printf("##Fila Vazia##\n");
		printf("##############\n");
	}else{
		f->inicio++;
		if (f->inicio == MAX ){
			f->inicio = 0;
		}
		f->qtd--;		
	}
}
void imprimi(Fila *f){
	if (f->qtd == 0){
		printf("##############\n");
		printf("##Fila Vazia##\n");
		printf("##############\n");
	}else{
		int a, i;
		for(a= 0,i = f->inicio; a < f->qtd; a++){
			
			printf("Analise do Cabelo \n");
			printf("Cor: %s \n", f->cliente[i].cor);
			printf("Tipo: %s \n", f->cliente[i].tipo);
			printf("Tamanho: %s \n\n", f->cliente[i].tam);
			i++;
			if (i == MAX) i=0;
		}
		printf("---------------------------------------");
		printf("\n");		
	}
	
}

