#include <stdio.h>
#include <stdlib.h>
#define MAXTAM 5

typedef struct  {
	int item[MAXTAM];
	int topo;
} pilha;

void inicia(pilha *p){
	p->topo = -1;
}
void insere (pilha *p, int b){
	if (p->topo == MAXTAM-1){
		printf(" Pilha Cheia"); 
	}else{
		p->topo++;
		p->item[p->topo]= b;
	}
}
void imprime (pilha *p){
	if (p->topo == -1){
		printf(" Pilha Vazia"); 
	}else{
		for(int i=0;i <= p->topo;i++){
			printf("%d,", p->item[i] );
		}
		printf("\n");
		
	}
}
void retira (pilha *p){
	if (p->topo == -1){
		printf(" Pilha Vazia"); 
	}else{
		p->topo--;
	}
}
