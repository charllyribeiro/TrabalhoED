#define MAX 5

typedef struct cabelo{
   char cor[20];
   char tipo[20];
   char tam[20];  
}Cabelo;

typedef struct barber{
	Cabelo cliente[MAX];
	int inicio;
}Barber;

void inicia(Barber *p){
	p->inicio = -1;
}

void inserir(Barber *p, char cor[], char tipo[], char tam[]){
	if (p->inicio == MAX-1){
		printf(" Pilha Cheia ");
	}else{
	
		Cabelo newCabelo;
		strcpy(newCabelo.cor,cor);
		strcpy(newCabelo.tipo,tipo);
		strcpy(newCabelo.tam,tam);
	
		p->inicio++;
		p->cliente[p->inicio] = newCabelo;
         }
}

void remover(Barber *p){
	if (p->inicio == -1){
		printf(" Pilha Vazia ");
	}else{
		p->inicio--;
	}
		
}

void verInicio(Barber *p){
	if (p->inicio == -1){
		printf(" Pilha Vazia ");
	}else{
		for(int x=0; x <= p->inicio; x++){
			printf("Analise do Cabelo \n\n");
			printf("Cor: %s \n", p->cliente[x].cor);
			printf("Tipo: %s \n", p->cliente[x].tipo );
			printf("Tamanho: %s \n", p->cliente[x].tam);
		}
		
		printf("---------------------------------------");
		printf("\n");
	}
}
	
