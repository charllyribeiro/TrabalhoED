#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cabelopilha.h"

int main(){
	
	Barber a, *x=&a;
	
	inicia(x);
	inserir(x,"preto" ,"armado","medio" );
	inserir(x,"azul" ,"sem vida","pequeno" );
    verInicio(x);
    remover(x);
    verInicio(x);
    inserir(x,"loiro" ,"crespo","grande" );
    inserir(x,"vermelho" ,"cacheado","medio" );
    inserir(x,"branco" ,"ondulado","medio" );
	verInicio(x);
	remover(x);
	remover(x);
	verInicio(x);
	inserir(x,"castanho" ,"liso","grande" );
	verInicio(x);
	
	
	return 0;
}
