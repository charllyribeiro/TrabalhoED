#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cabelofila.h"

int main(){
	Fila x, *a=&x;
	
	iniciar(a);
	
	retirar(a);
	imprimir(a);
	
	inserir(a,"azul", "cacheado", "medio");
	inserir(a,"vermelho", "ondulado", "longo");
	inserir(a,"loiro", "liso", "curto");
	imprimir(a);
	retirar(a);
	imprimir(a);
	inserir(a,"preto","crespo","curto");
	inserir(a,"roxo", "liso", "medio");
	imprimir(a);
	inserir(a,"castanho", "liso", "longo");
	imprimir(a);
	retirar(a);
	imprimir(a);
	inserir(a,"branco", "ondulado", "medio");
	inserir(a,"ruiva", "liso", "longo");
	inserir(a,"cinza", "ondulado", "curto");
	imprimir(a);
	retirar(a);
	imprimir(a);
	inserir(a,"loiro medio", "liso", "medio");
	imprimir(a);
	
	
	return 0;
}
