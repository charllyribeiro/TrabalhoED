#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cabelocirc.h"

int main(){
	
	Fila x, *a=&x;
	
	inicia(a);
	
	imprimi(a);
	retira(a);
	
	insere(a,"roxo", "liso", "medio");
	insere(a,"preto","crespo","curto");
	insere(a,"vermelho", "ondulado", "longo");
	insere(a,"loiro", "liso", "curto");
	insere(a,"azul", "cacheado", "medio");
	imprimi(a);
	
	retira(a);
	retira(a);
	retira(a);
	retira(a);
	imprimi(a);
	
	insere(a,"preto","crespo","curto");
	imprimi(a);
	
	insere(a,"vermelho", "ondulado", "longo");
	insere(a,"loiro", "liso", "curto");
	imprimi(a);
	
	retira(a);
	imprimi(a);
	
	insere(a,"roxo", "liso", "medio");
	insere(a,"castanho", "liso", "longo");
	imprimi(a);
	insere(a,"branco", "ondulado", "medio");
	

	retira(a);
	retira(a);
	imprimi(a);
	
	
	 
	
	return 0;
}
