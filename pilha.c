#include "pilha.h"

int main(){
	pilha a, *x=&a;
	
	inicia(x);
	insere(x,3);
	insere(x,8);
	imprime(x);
	insere(x,4);
	insere(x,1);
	imprime(x);
	retira(x);
	imprime(x);
	retira(x);
	retira(x);
	imprime(x);
	insere(x,9);
	imprime(x);
	
	return 0;
}
