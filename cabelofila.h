#define SIZE 5

typedef struct cabelo{
   char cor[20];
   char tipo[20];
   char tam[20];  
}Cabelo;

typedef struct fila{
	Cabelo cliente[SIZE];
	int inicio, fim;
}Fila;

void iniciar(Fila *f){
	f->inicio = 0;
	f->fim = -1;
}
void inserir(Fila *f, char cor[], char tipo[], char tam[]){
	if (f->fim == SIZE-1){
		printf(" ###Fila Cheia### \n");
	}else{
		Cabelo newCliente;
		strcpy(newCliente.cor,cor);
		strcpy(newCliente.tipo, tipo);
		strcpy(newCliente.tam, tam);
	
		f->fim++;
		f->cliente[f->fim] = newCliente;	
	}
	
}
void retirar(Fila *f){
	if (f->fim < f->inicio){
		printf("##Fila Vazia## \n");
	}else{
		f->inicio++;
	}
	
}
void imprimir(Fila *f){
	if (f->fim < f->inicio){
		printf("##Fila Vazia## \n");
	}else{
		for(int x=f->inicio; x <= f->fim; x++){
		
			printf("Analise do Cabelo \n");
			printf("Cor: %s \n", f->cliente[x].cor);
			printf("Tipo: %s \n", f->cliente[x].tipo);
			printf("Tamanho: %s \n\n", f->cliente[x].tam);
		}
		printf("---------------------------------------");
		printf("\n");
	}
}
