#define MAX 5

typedef struct fila{
  int elemento[MAX];
  int inicio;
  int fim;
  int qtd;
}Fila;

void inicia(Fila *f){
  f->inicio = 0;
  f->fim = -1;
  f-> qtd = 0;
}
void insere(Fila *f, int num){
	if(f->fim == MAX-1)
    	f->fim = -1;
  	if (f->qtd == MAX){
    printf("Fila Cheia\n");
  }else{
    f->fim++;
    f->elemento[f->fim] = num;
    f->qtd++;
  }
}
  
void retirar(Fila *f){
	
	if(f->inicio == MAX)
    	f->inicio = 0;
	if (f->qtd == 0){
    printf("Fila Vazia\n");
  	}else{
  		f->inicio++;
		f->qtd--;
	  }

  
}
void imprimir(Fila *f){
	if (f->qtd == 0){
    printf("Fila Vazia\n");
  	}else{
  int a, i;
    for(a=0, i= f->inicio; a < f->qtd; a++){
		
      	printf("%d, ",f->elemento[i++] );
      
      	if( i == MAX) i=0;      

    }
    printf("\n");
}

}
