#include <stdio.h>
#include <stdlib.h>
#include "intcircular.h"

int main(){

  Fila a, *x=&a;

  inicia(x);
  
  retirar(x);
  imprimir(x);
  
  insere(x,3);
  imprimir(x);

  insere(x,5);
  insere(x,7);
  insere(x,2);
  imprimir(x);

  retirar(x);
  imprimir(x);

  insere(x,1);
  insere(x,9);
  imprimir(x);
  insere(x,10);

  retirar(x);
  retirar(x);
  imprimir(x);
  insere(x,10);
  imprimir(x);

  




  return 0;
}
